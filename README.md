# PostmanCollections

- Postman simplifies API testing by allowing you to send HTTP requests such as Post, Get, Put, Patch & Delete API's and receive responses. You can create and organize automate tests, and validate the API's functionality.
- You can organize your API requests into collections,making it easier to manage and run multiple requests in a sequence.
- Postman enables you to define and use environment variables to parameterize your requests. This is useful for testing against different environments.
- You can write scripts that run before a request is sent.This can be used to set up data, authenticate, or perform other tasks necessary for the request.
- You can import data files (e.g., CSV, JSON) and use them to parameterize your API requests.This is helpful for testing various input scenarios.
- Postman's CLI tool, called Newman, allows you to run collections and environments from the command line, making it suitable for automated testing.

## Projects:

1. API_TestCases
2. Data_Driven_Testing
3. Dynamic Variables
4. Request Chaining

## API_TestCases: 

Automated test cases for various API requests (Post, Get, Put, Patch, Delete) using Postman and Newman for automation. It employs environment variables and generates HTML reports.

## Data_Driven_Testing:

Focuses on Data-Driven Testing with CSV and JSON files. Data is loaded into the collection from these files, used in request body parameters, and executed with the Collection Runner.

## Dynamic Variables: 

Utilizes Postman's faker library to create dynamic variables used in collections, global, and environment variables. This allows for the generation of multiple sets of dummy data.

## Request Chaining: 

Addresses environment instability issues by implementing request chaining for retries. It's a workaround for unstable API requests that need multiple attempts to obtain accurate results, while the issue is reported to the infrastructure team.
